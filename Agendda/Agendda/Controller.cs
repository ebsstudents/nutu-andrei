﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agendda
{
    class Controller
    {
        FileStream fisier = null;
        StreamReader source = null;
        StreamWriter dest = null;
        List<Inregistrare> inregistrare;
        int nrIntrari = 0;
        String line;

        public void OpenFile(String numeFisier)
        {
            if (fisier != null)
                fisier.Close();
            fisier = new FileStream(numeFisier, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            source = new StreamReader(fisier);
            dest = new StreamWriter(fisier);
        }

        public List<Inregistrare> CitesteInregistrari()
        {
            fisier.Position = 0;
            inregistrare = new List<Inregistrare>();
            while((line = source.ReadLine()) != null)
            {
                string[] tokens = line.Split(',');
                inregistrare.Add(new Inregistrare(tokens[0], tokens[1], tokens[2]));
            }
            return inregistrare;
        }

        public bool ScrieInregistrare(String nume, String telefon, String email)
        {
            if (fisier != null)
            {
                String line = nume + ',' + telefon + ',' + email;
                dest.WriteLine(line);
                dest.Flush();
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ModificaInregistrare(String phoneNumber, String nume, String telefon, String email)
        {
            fisier.SetLength(0);
            fisier.Flush();
            for (int i = 0; i < inregistrare.Count; i++)
                if (inregistrare[i].getTelefon().Equals(phoneNumber))
                {
                    inregistrare[i].setNume(nume);
                    inregistrare[i].setTelefon(telefon);
                    inregistrare[i].setNume(email);
                }
            for (int i = 0; i < inregistrare.Count; i++)
            {
                dest.WriteLine(inregistrare[i].getNume() + "," + inregistrare[i].getTelefon() + "," + inregistrare[i].getEmail());
                dest.Flush();
            }
        }

        public void StergeInregistrare(String phoneNumber)
        {
            fisier.SetLength(0);
            fisier.Flush();
            for (int i = 0; i < inregistrare.Count; i++)
            {
                if (inregistrare[i].getTelefon().Equals(phoneNumber) != true)
                {
                    dest.WriteLine(inregistrare[i].getNume() + "," + inregistrare[i].getTelefon() + "," + inregistrare[i].getEmail());
                    dest.Flush();
                }
            }
        }
    }
}
