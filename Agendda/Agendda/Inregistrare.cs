﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agendda
{
    class Inregistrare
    {
        String nume;
        String telefon;
        String email;

        public Inregistrare(String nume, String telefon, String email) 
        {
            this.nume = nume;
            this.telefon = telefon;
            this.email = email;
        }

        public String getNume()
        {
            return this.nume;
        }

        public String getTelefon()
        {
            return this.telefon;
        }

        public String getEmail()
        {
            return this.email;
        }

        public void setNume(String nume)
        {
            this.nume = nume;
        }

        public void setTelefon(String telefon)
        {
            this.telefon = telefon;
        }

        public void setEmail(String email)
        {
            this.email = email;
        }
    }
}
