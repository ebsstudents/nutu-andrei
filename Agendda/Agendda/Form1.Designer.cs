﻿namespace Agendda
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agenda = new System.Windows.Forms.DataGridView();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nrTel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.boxNume = new System.Windows.Forms.TextBox();
            this.boxEmail = new System.Windows.Forms.TextBox();
            this.boxTelefon = new System.Windows.Forms.TextBox();
            this.labelNume = new System.Windows.Forms.Label();
            this.labelTelefon = new System.Windows.Forms.Label();
            this.labelEmail = new System.Windows.Forms.Label();
            this.btnAdaugare = new System.Windows.Forms.Button();
            this.btnModificare = new System.Windows.Forms.Button();
            this.btnStergere = new System.Windows.Forms.Button();
            this.fisier = new System.Windows.Forms.OpenFileDialog();
            this.btnAgenda = new System.Windows.Forms.Button();
            this.filterText = new System.Windows.Forms.TextBox();
            this.filterButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.agenda)).BeginInit();
            this.SuspendLayout();
            // 
            // agenda
            // 
            this.agenda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.agenda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.agenda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.agenda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.name,
            this.nrTel,
            this.email});
            this.agenda.Location = new System.Drawing.Point(12, 39);
            this.agenda.Name = "agenda";
            this.agenda.RowHeadersWidth = 25;
            this.agenda.ShowCellErrors = false;
            this.agenda.ShowCellToolTips = false;
            this.agenda.ShowEditingIcon = false;
            this.agenda.ShowRowErrors = false;
            this.agenda.Size = new System.Drawing.Size(456, 195);
            this.agenda.TabIndex = 0;
            this.agenda.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.agenda_Click);
            this.agenda.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.agenda_Click);
            // 
            // name
            // 
            this.name.HeaderText = "Nume";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // nrTel
            // 
            this.nrTel.HeaderText = "Numar Telefon";
            this.nrTel.Name = "nrTel";
            this.nrTel.ReadOnly = true;
            this.nrTel.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            this.email.ReadOnly = true;
            this.email.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // boxNume
            // 
            this.boxNume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.boxNume.Location = new System.Drawing.Point(531, 39);
            this.boxNume.Name = "boxNume";
            this.boxNume.Size = new System.Drawing.Size(126, 20);
            this.boxNume.TabIndex = 1;
            // 
            // boxEmail
            // 
            this.boxEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.boxEmail.Location = new System.Drawing.Point(531, 143);
            this.boxEmail.Name = "boxEmail";
            this.boxEmail.Size = new System.Drawing.Size(126, 20);
            this.boxEmail.TabIndex = 3;
            // 
            // boxTelefon
            // 
            this.boxTelefon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.boxTelefon.Location = new System.Drawing.Point(531, 91);
            this.boxTelefon.Name = "boxTelefon";
            this.boxTelefon.Size = new System.Drawing.Size(126, 20);
            this.boxTelefon.TabIndex = 2;
            this.boxTelefon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.boxTelefon_KeyPress);
            // 
            // labelNume
            // 
            this.labelNume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNume.AutoSize = true;
            this.labelNume.Location = new System.Drawing.Point(580, 23);
            this.labelNume.Name = "labelNume";
            this.labelNume.Size = new System.Drawing.Size(35, 13);
            this.labelNume.TabIndex = 4;
            this.labelNume.Text = "Nume";
            // 
            // labelTelefon
            // 
            this.labelTelefon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTelefon.AutoSize = true;
            this.labelTelefon.Location = new System.Drawing.Point(575, 75);
            this.labelTelefon.Name = "labelTelefon";
            this.labelTelefon.Size = new System.Drawing.Size(43, 13);
            this.labelTelefon.TabIndex = 5;
            this.labelTelefon.Text = "Telefon";
            // 
            // labelEmail
            // 
            this.labelEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(580, 127);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(32, 13);
            this.labelEmail.TabIndex = 6;
            this.labelEmail.Text = "Email";
            // 
            // btnAdaugare
            // 
            this.btnAdaugare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdaugare.Location = new System.Drawing.Point(474, 177);
            this.btnAdaugare.Name = "btnAdaugare";
            this.btnAdaugare.Size = new System.Drawing.Size(75, 23);
            this.btnAdaugare.TabIndex = 7;
            this.btnAdaugare.Text = "Adaugare";
            this.btnAdaugare.UseVisualStyleBackColor = true;
            this.btnAdaugare.Click += new System.EventHandler(this.btnAdaugare_Click);
            // 
            // btnModificare
            // 
            this.btnModificare.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModificare.Location = new System.Drawing.Point(555, 177);
            this.btnModificare.Name = "btnModificare";
            this.btnModificare.Size = new System.Drawing.Size(75, 23);
            this.btnModificare.TabIndex = 8;
            this.btnModificare.Text = "Modificare";
            this.btnModificare.UseVisualStyleBackColor = true;
            this.btnModificare.Click += new System.EventHandler(this.btnModificare_Click);
            // 
            // btnStergere
            // 
            this.btnStergere.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStergere.Location = new System.Drawing.Point(636, 177);
            this.btnStergere.Name = "btnStergere";
            this.btnStergere.Size = new System.Drawing.Size(75, 23);
            this.btnStergere.TabIndex = 9;
            this.btnStergere.Text = "Stergere";
            this.btnStergere.UseVisualStyleBackColor = true;
            this.btnStergere.Click += new System.EventHandler(this.btnStergere_Click);
            // 
            // fisier
            // 
            this.fisier.FileName = "openFileDialog1";
            // 
            // btnAgenda
            // 
            this.btnAgenda.Location = new System.Drawing.Point(12, 7);
            this.btnAgenda.Name = "btnAgenda";
            this.btnAgenda.Size = new System.Drawing.Size(117, 23);
            this.btnAgenda.TabIndex = 10;
            this.btnAgenda.Text = "Selecteaza Agenda";
            this.btnAgenda.UseVisualStyleBackColor = true;
            this.btnAgenda.Click += new System.EventHandler(this.btnAgenda_Click);
            // 
            // filterText
            // 
            this.filterText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filterText.Location = new System.Drawing.Point(135, 10);
            this.filterText.Name = "filterText";
            this.filterText.Size = new System.Drawing.Size(252, 20);
            this.filterText.TabIndex = 11;
            // 
            // filterButton
            // 
            this.filterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.filterButton.Location = new System.Drawing.Point(393, 9);
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size(75, 23);
            this.filterButton.TabIndex = 12;
            this.filterButton.Text = "Cauta";
            this.filterButton.UseVisualStyleBackColor = true;
            this.filterButton.Click += new System.EventHandler(this.filterButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 246);
            this.Controls.Add(this.filterButton);
            this.Controls.Add(this.filterText);
            this.Controls.Add(this.btnAgenda);
            this.Controls.Add(this.btnStergere);
            this.Controls.Add(this.btnModificare);
            this.Controls.Add(this.btnAdaugare);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.labelTelefon);
            this.Controls.Add(this.labelNume);
            this.Controls.Add(this.boxEmail);
            this.Controls.Add(this.boxTelefon);
            this.Controls.Add(this.boxNume);
            this.Controls.Add(this.agenda);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.agenda)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView agenda;
        private System.Windows.Forms.TextBox boxNume;
        private System.Windows.Forms.TextBox boxEmail;
        private System.Windows.Forms.TextBox boxTelefon;
        private System.Windows.Forms.Label labelNume;
        private System.Windows.Forms.Label labelTelefon;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Button btnAdaugare;
        private System.Windows.Forms.Button btnModificare;
        private System.Windows.Forms.Button btnStergere;
        private System.Windows.Forms.OpenFileDialog fisier;
        private System.Windows.Forms.Button btnAgenda;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn nrTel;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.TextBox filterText;
        private System.Windows.Forms.Button filterButton;
    }
}

