﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agendda
{
    public partial class Form1 : Form
    {
        Controller controller = new Controller();
        List<Inregistrare> inregistrare;
        String selected;
        public Form1()
        {
            InitializeComponent();
            fisier = new OpenFileDialog();
        }

        private void IncarcaAgenda(String numeCautat = "")
        {
            agenda.Rows.Clear();
            agenda.Refresh();
            for (int i = 0; i < inregistrare.Count; i++)
            {
                if (inregistrare[i].getNume().Contains(numeCautat))
                {
                    agenda.Rows.Add(inregistrare[i].getNume(), inregistrare[i].getTelefon(), inregistrare[i].getEmail());
                }
            } 
        }

        private void btnAgenda_Click(object sender, EventArgs e)
        {
            fisier = new OpenFileDialog();
            fisier.ShowDialog();
            controller.OpenFile(fisier.FileName);
            inregistrare = controller.CitesteInregistrari();
            IncarcaAgenda();
        }

        private void btnAdaugare_Click(object sender, EventArgs e)
        {
            bool rez = false;
            bool neexistent = true;
            if (boxNume.Text != "")
            {
                if (boxTelefon.TextLength == 10)
                {
                    if (boxEmail.Text != "" && boxEmail.Text.Contains('@') && boxEmail.Text.Contains(".com"))
                    {
                        for (int i = 0; i < inregistrare.Count; i++)
                            if (inregistrare[i].getTelefon().Equals(boxTelefon.Text))
                                neexistent = false;
                        if (neexistent == true)
                        {
                            rez = controller.ScrieInregistrare(boxNume.Text, boxTelefon.Text, boxEmail.Text);
                            if (rez == false)
                            {
                                MessageBox.Show("Niciun fisier nu este deschis pentru a memora inregistrarea", "ERROR!");
                                btnAgenda.PerformClick();
                            }
                            else
                            {
                                boxNume.Text = "";
                                boxTelefon.Text = "";
                                boxEmail.Text = "";
                                inregistrare = controller.CitesteInregistrari();
                                IncarcaAgenda();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Exista deja o inregistrare cu acest numar de telefon", "ERROR!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Emailul trebuie sa fie valid!", "ERROR!");
                    }
                }
                else
                {
                    MessageBox.Show("Numarul de telefon trebuie sa aiba 10 cifre!", "ERROR!");
                }
            }
            else
            {
                MessageBox.Show("Niciun Nume introdus!", "ERROR!");
            }
        }

        private void agenda_Click(object sender, DataGridViewCellMouseEventArgs e)
        {
            int index = agenda.CurrentCell.RowIndex;
            if (inregistrare == null)
                MessageBox.Show("Incarca Agenda!", "ERROR!");
            else
            {
                this.boxNume.Text = agenda.Rows[index].Cells[0].Value.ToString();
                selected = this.boxTelefon.Text = agenda.Rows[index].Cells[1].Value.ToString();
                this.boxEmail.Text = agenda.Rows[index].Cells[2].Value.ToString();
            }
        }

        private void btnModificare_Click(object sender, EventArgs e)
        {
                controller.ModificaInregistrare(selected, boxNume.Text, boxTelefon.Text, boxEmail.Text);
                inregistrare = controller.CitesteInregistrari();
                IncarcaAgenda();
        }

        private void btnStergere_Click(object sender, EventArgs e)
        {
            if (agenda.CurrentCell != null)
            {
                controller.StergeInregistrare(boxTelefon.Text);
                inregistrare = controller.CitesteInregistrari();
                IncarcaAgenda();
            }
            else
            {
                MessageBox.Show("Selecteaza o inregistrare!", "ERROR!");
            }
        }

        private void boxTelefon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
            {
                e.Handled = true;
                MessageBox.Show("Numarul de telefon trebuie sa contina doar numere!");
            }
        }

        private void filterButton_Click(object sender, EventArgs e)
        {
            IncarcaAgenda(filterText.Text);
        }
    }
}
